module.exports = function(server){
    server.route({
        method:'POST',
        path: '/sms/checkin',
        handler: async function(request,response){
            var reservation_id = request.payload.reservation_id
            console.log(reservation_id)
            var snapshot = await App.db.collection('bookings').where('bookingNo', '==', reservation_id).get();
            if (!snapshot.empty){
                var booking = snapshot.docs[0].data()
                if (booking !== undefined && booking.ticketNo != undefined && booking.ticketNo.trim() !== '' ){
                    var ticketNumber = booking.ticketNo
                    var phoneNumber = '+1' + booking.customerPhone.replace('(','').replace(')','').replace('-','').replace(' ','')
                    var message = await App.twilio.messages.create({
                        body: 'Ticket number ' +ticketNumber+ '.Your vehicle is now parked at midway SpeedPark. Please enter your ticket number when you return. OPN: '+phoneNumber,
                        from: '+19412412040',
                        to: '+18133568208'
                    })
                    return {'message' : 'Message Send with SID: '+message.sid}
                }
                return {'message': 'Invalid Booking. Invalid ticket number'}
            }
            return {'message': 'Invalid Booking. No document found'}
        }
    })
    
    server.route({
        method:'POST',
        path: '/sms/receive',
        handler: async function(request,response){
            var ticketNo = request.payload.Body
            if(Number(ticketNo)){
                var snapshot = await App.db.collection('bookings').where('ticketNo', '==', ticketNo).get();
                if (!snapshot.empty){
                    var bookingId = snapshot.docs[0].id
                    if (bookingId !== undefined){
                        var bookingRef = App.db.collection('bookings').doc(bookingId)
                        bookingRef.set({'bookingStatus':'Active'}, {merge:true})
                        return '<Response><Message>Midway Parking spot has received your request for ticket '+ticketNo+'. For immediate assistance, please call/text 773-585-4200. Please exit from the GARAGE CENTER and proceed to the CHARTER BUS STOP.</Message></Response>'
                    }
                    return {'message':'Invalid Ticket Number. Invalid Booking Id.'}
                }
                return {'message':'Invalid Ticket Number. Ticket is not in the system.'}
            }
            return {'message':'Invalid Ticket Number'}
        }
    })
}