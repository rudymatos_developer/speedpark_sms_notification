'use strict';

var env            = process.env.NODE_ENV || 'development',
    hapi           = require('hapi'),
    packageJSON    = require('../package.json'),
    sms            = require('../routes/sms'),
    firebaseConfig = require('./firebase_config.json'),
    twilioConfig   = require('./twilio_config.json'),
    twilio         = require('twilio')(twilioConfig.accoundSID, twilioConfig.authToken),
    admin          =  require('firebase-admin');

console.log('Loading App in '+env+' mode.')
var server = new hapi.Server({port: process.env.PORT || 3000, routes: {cors: true}});
admin.initializeApp({credential: admin.credential.cert(firebaseConfig)})

global.App = {
    hapiServer : server,
    port: process.env.PORT || 3000,
    env: env,
    twilioConfig: twilioConfig, 
    twilio: twilio,
    db: admin.firestore(),
    version : packageJSON.version,
    start: async function() {
        sms(App.hapiServer)
        await App.hapiServer.start()
        console.log('Server running on '+server.info.uri)
    }
}

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});
